import socket


def main():
    host = "127.0.0.1"  # IP address of the server
    port = 12345       # Port to connect to

    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client_socket.connect((host, port))

    while True:
        data = client_socket.recv(1024).decode()
        if not data:
            break
        print("Received from server:", data)

        num = int(data) + 1
        client_socket.send(str(num).encode())

    client_socket.close()


if __name__ == "__main__":
    main()
