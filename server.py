import socket

def main():
    host = "127.0.0.1"  # IP address of the server
    port = 12345       # Port to listen on

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind((host, port))
    server_socket.listen(1)
    print("Server is listening...")

    connection, address = server_socket.accept()
    print("Connected by:", address)

    num = 1
    while num <= 100:
        connection.send(str(num).encode())
        data = connection.recv(1024).decode()
        print("Received:", data)
        num = int(data) + 1

    connection.close()

if __name__ == "__main__":
    main()
